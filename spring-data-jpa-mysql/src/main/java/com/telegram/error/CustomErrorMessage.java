package com.telegram.error;

public class CustomErrorMessage {
    private String errorMessage;
    private int statusCode;


    @Override
    public String toString() {
        return "CustomErrorMessage{" +
                "errorMessage='" + errorMessage + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public CustomErrorMessage setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public CustomErrorMessage setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }
}
