package com.telegram.service;

import com.telegram.model.Contacts;
import com.telegram.model.Report;
import com.telegram.model.TelegramUser;

import java.util.List;

public interface Service {
    public TelegramUser findUser(TelegramUser user);
    public Contacts saveContactsReport(Contacts contacts);
    public Contacts updateReport(Contacts contacts);
    public List<Report> getReportForAllUsers();
}
