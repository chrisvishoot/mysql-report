package com.telegram.service;

import com.telegram.model.Contacts;
import com.telegram.model.Report;
import com.telegram.model.TelegramUser;
import com.telegram.repository.ContactsRepository;
import com.telegram.repository.ReportRepository;
import com.telegram.repository.TelegramRepository;
import com.telegram.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
public class ServiceImpl implements Service {
    @Autowired
    TelegramRepository telegramRepository;

    @Autowired
    ContactsRepository contactsRepository;

    @Autowired
    ReportRepository reportRepository;


    public Contacts saveContactsReport(Contacts contacts) {
        String currentDate = Utility.getCurrentDate();
        contacts.setTimeStamp(currentDate);
        return contactsRepository.save(contacts);
    }


    public List<Contacts> getAllContactsByTelegramId(Long telegramId) {

        return contactsRepository.findAllByTelegramUserId(telegramId);
    }

    public TelegramUser findUser(TelegramUser user) {
        TelegramUser userResult = null;
        Optional<TelegramUser> optionalTelegramUser = null;
        if(user.getIdTelegramUser() != null) {
            optionalTelegramUser =
                    telegramRepository.findTelegramUserByIdTelegramUser(user.getIdTelegramUser());
        } else if(user.getTelegramUserName() != null && !user.getTelegramUserName().isEmpty()) {
            optionalTelegramUser = telegramRepository.
                            findTelegramUserByTelegramUserName(user.getTelegramUserName());
        }
        try {
            userResult = optionalTelegramUser.get();
        } catch(NoSuchElementException notFound) {
            System.out.println("Unable to find user " + user.getTelegramUserName());
        }

        return userResult;
    }

    public List<Report> getReportForAllUsers() {
        List<Report> reports = this.reportRepository.getUserReport();
        return reports;

    }

    public Contacts updateReport(Contacts contacts) {
        String currentDate = Utility.getCurrentDate();
        contacts.setTimeStamp(currentDate);
        Contacts result = contactsRepository.save(contacts);
        return result;
    }
}
