package com.telegram.model;

import javax.persistence.*;

@Entity
public class Contacts {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long reportId;

    private Long numberOfContacts;
    private Long promisedToGo;

    private Long telegramUserId;
    private String timeStamp;


    public Contacts() {}

    public Long getNumberOfContacts() {
        return numberOfContacts;
    }

    public Contacts setNumberOfContacts(Long numberOfContacts) {
        this.numberOfContacts = numberOfContacts;
        return this;
    }

    public Long getPromisedToGo() {
        return promisedToGo;
    }

    public Contacts setPromisedToGo(Long promisedToGo) {
        this.promisedToGo = promisedToGo;
        return this;
    }

    public Long getTelegramUserId() {
        return telegramUserId;
    }

    public Contacts setTelegramUserId(Long telegramUserId) {
        this.telegramUserId = telegramUserId;
        return this;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public Contacts setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public Long getReportId() {
        return reportId;
    }

    public Contacts setReportId(Long reportId) {
        this.reportId = reportId;
        return this;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "reportId=" + reportId +
                ", numberOfContacts=" + numberOfContacts +
                ", promisedToGo=" + promisedToGo +
                ", telegramUserId=" + telegramUserId +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
