package com.telegram.model;


import javax.persistence.*;
import java.util.List;

@Entity
public class TelegramUser {
    @Id
    private Long idTelegramUser;
    private String telegramUserName;


    public TelegramUser(String userName) {
        this.telegramUserName = userName;
    }

    public TelegramUser() {}

    public Long getIdTelegramUser() {
        return idTelegramUser;
    }

    public void setIdTelegramUser(Long idTelegramUser) {
        this.idTelegramUser = idTelegramUser;
    }

    public String getTelegramUserName() {
        return telegramUserName;
    }

    public void setTelegramUserName(String telegramUserName) {
        this.telegramUserName = telegramUserName;
    }

    @Override
    public String toString() {
        return "TelegramUser{" +
                "idTelegramUser=" + idTelegramUser +
                ", telegramUserName='" + telegramUserName + '\'' +
                '}';
    }
}
