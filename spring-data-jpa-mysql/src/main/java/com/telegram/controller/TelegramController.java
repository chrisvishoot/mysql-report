package com.telegram.controller;


import com.amazonaws.Response;
import com.telegram.error.CustomErrorMessage;
import com.telegram.model.Contacts;
import com.telegram.model.Report;
import com.telegram.model.TelegramUser;
import com.telegram.repository.ReportRepository;
import com.telegram.repository.TelegramRepository;
import com.telegram.service.ServiceImpl;
import com.telegram.verify.VerifyInputImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class TelegramController {

    @Autowired
    ServiceImpl service;

    @Autowired
    TelegramRepository telegramRepository;

    @Autowired
    VerifyInputImpl verifyInput;



    @PostMapping("/createTelegramUser")
    public ResponseEntity<?> createTelegramUser(@RequestBody TelegramUser telegramUser) {
        CustomErrorMessage errorMessage = verifyInput.verifyTelegramUser(telegramUser);
        if(errorMessage != null) {
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        TelegramUser user = telegramRepository.save(telegramUser);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


    @PostMapping("/checkIfUserExists")
    public ResponseEntity<?> checkIfUserExists(@RequestBody TelegramUser telegramUser) {
        CustomErrorMessage errorMessage = verifyInput.verifyTelegramUser(telegramUser);
        if(errorMessage != null) {
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        TelegramUser foundUser = service.findUser(telegramUser);
        if(foundUser == null) {
            CustomErrorMessage message = new CustomErrorMessage();
            message.setErrorMessage("Unable to find user " + telegramUser.getTelegramUserName());
            message.setStatusCode(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(foundUser, HttpStatus.OK);
    }


    @PostMapping("/createReport")
    public ResponseEntity<?> createReport(@RequestBody Contacts contacts) {
        CustomErrorMessage errorMessage = verifyInput.verifyContacts(contacts);
        if(errorMessage != null) {
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        Contacts contactsReport = service.saveContactsReport(contacts);
        return new ResponseEntity<>(contactsReport, HttpStatus.OK);

    }


    @GetMapping("/greeting")
    public String greeting() {
        return "hello world";
    }

    @GetMapping("/getAllUsers")
    public ResponseEntity<?> getAllUsers() {
        List<TelegramUser> telegramUsers = telegramRepository.getAllUsers();
        return new ResponseEntity<>(telegramUsers, HttpStatus.OK);
    }

    @PostMapping("/getReportForUser")
    public ResponseEntity<?> getReportForUser(@RequestBody TelegramUser telegramUser) {
        CustomErrorMessage errorMessage = verifyInput.verifyTelegramUser(telegramUser);
        if(errorMessage != null) {
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        List<Contacts> contactsList = service.getAllContactsByTelegramId(telegramUser.getIdTelegramUser());
        return new ResponseEntity<>(contactsList, HttpStatus.OK);
    }

    @GetMapping("/getAllReports")
    public ResponseEntity<?> getAllReports() {
        List<Report> reports = service.getReportForAllUsers();
        return  new ResponseEntity<>(reports, HttpStatus.OK);
    }

    @PostMapping("/updateContacts")
    public ResponseEntity<?> updateContacts(@RequestBody Contacts contacts) {
        CustomErrorMessage errorMessage = verifyInput.verifyContacts(contacts);
        if(errorMessage != null) {
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        Contacts updated = service.updateReport(contacts);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

}
