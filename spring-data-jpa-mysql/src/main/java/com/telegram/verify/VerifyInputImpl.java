package com.telegram.verify;

import com.telegram.error.CustomErrorMessage;
import com.telegram.model.Contacts;
import com.telegram.model.TelegramUser;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class VerifyInputImpl implements VerifyInput {

    public CustomErrorMessage verifyTelegramUser(TelegramUser user) {
        CustomErrorMessage errorMessage = null;
        if(user.getTelegramUserName() == null && user.getIdTelegramUser() == null
                && user.getTelegramUserName().isEmpty()) {
            errorMessage.setErrorMessage("Invalid Telegram User Input");
            errorMessage.setStatusCode(HttpStatus.BAD_REQUEST.value());

        }
        return errorMessage;
    }
    public CustomErrorMessage verifyContacts(Contacts contacts) {
        CustomErrorMessage errorMessage = null;
        StringBuilder sb = new StringBuilder();
        boolean hasError = false;
        if(contacts.getTelegramUserId() == null) {
            sb.append("Invalid user id");
            sb.append(System.lineSeparator());
            hasError = true;
        }
        if(contacts.getNumberOfContacts() == null && contacts.getNumberOfContacts() < 0) {
            sb.append("Invalid contacts");
            sb.append(System.lineSeparator());
            hasError = true;
        }
        if(contacts.getPromisedToGo() == null && contacts.getPromisedToGo() < 0) {
            sb.append("Invalid promised to go");
            sb.append(System.lineSeparator());
            hasError = true;
        }
        if(hasError) {
            errorMessage = new CustomErrorMessage();
            errorMessage.setStatusCode(HttpStatus.BAD_REQUEST.value());
            errorMessage.setErrorMessage(sb.toString());
        }
        return errorMessage;
    }

}
