package com.telegram.verify;

import com.telegram.error.CustomErrorMessage;
import com.telegram.model.Contacts;
import com.telegram.model.TelegramUser;

public interface VerifyInput {
    public CustomErrorMessage verifyTelegramUser(TelegramUser user);
    public CustomErrorMessage verifyContacts(Contacts contacts);
}
