package com.telegram.repository;

import com.telegram.model.Contacts;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContactsRepository extends CrudRepository<Contacts, Long> {
    List<Contacts> findAllByTelegramUserId(Long telegramId);


}
