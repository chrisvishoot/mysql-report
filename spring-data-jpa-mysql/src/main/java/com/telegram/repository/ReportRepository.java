package com.telegram.repository;

import com.telegram.model.Report;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReportRepository extends CrudRepository<Report, Long> {
    final static String getReportQuery = "SELECT * FROM helloWorld.telegram_user JOIN helloWorld.contacts ON helloWorld.telegram_user.id_telegram_user = helloWorld.contacts.telegram_user_id";

    @Query(value = getReportQuery, nativeQuery = true)
    List<Report> getUserReport();
}
