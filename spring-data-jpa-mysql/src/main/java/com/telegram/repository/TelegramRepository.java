package com.telegram.repository;

import com.telegram.model.TelegramUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TelegramRepository extends CrudRepository<TelegramUser, Long> {
    @Query(value="SELECT * FROM helloWorld.telegram_user", nativeQuery=true)
    List<TelegramUser> getAllUsers();


    Optional<TelegramUser> findTelegramUserByIdTelegramUser(Long id);

    Optional<TelegramUser> findTelegramUserByTelegramUserName(String telegramUserName);







}
